import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError, Observable, of } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class GraphiquesService {
  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  };
  constructor(private http: HttpClient) { }

    baseurlnat: string = "https://springrestapi1.herokuapp.com/";

    getStatTeamByIdNat(id:any): Observable<any>{
   
      return this.http.get<any>(this.baseurlnat + 'footlive/player/statnat/'+`${id}`).pipe(
        
        catchError(this.handleError<any>(`getteam id=${id}`))
      );;
  
    }

    getStatTeamByIdAge(id:any): Observable<any>{
   console.log('footlive/player/statage/'+`${id}`);
      return this.http.get<any>(this.baseurlnat + 'footlive/player/statage/'+`${id}`).pipe(
        
        catchError(this.handleError<any>(`getteam id=${id}`))
      );;
  
    }


    private handleError<T>(operation = 'operation', result?: T) {
      return (error: any): Observable<T> => {
  
        // TODO: send the error to remote logging infrastructure
        console.error(error); // log to console instead
  
        // TODO: better job of transforming error for user consumption
        this.log(`${operation} failed: ${error.message}`);
  
        // Let the app keep running by returning an empty result.
        return of(result as T);
      };
    }
  
    /** Log a HeroService message with the MessageService */
    private log(message: string) {
      console.log(message);
    }



  
}
