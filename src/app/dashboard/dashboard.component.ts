
import { Component, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { GreetingService } from '../greeting.service';
import { ResponseAdmin } from '../Model/ResponseAdmin.model';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  users:ResponseAdmin[]=[];


  constructor(private greetingService:GreetingService) { }
  
  ngOnInit(): void {
    this.getAllUsers();
  }


  getAllUsers(): void {
    this.greetingService.getUsers().subscribe(data=>{
      console.log(data);
      this.users=data;
      
      
    });
  };
  OnClick(responseAdmin:ResponseAdmin){
    console.log(responseAdmin)
this.greetingService.banUser(responseAdmin);
if(responseAdmin.auth===true){
  responseAdmin.auth=false
}else{
responseAdmin.auth=true
}

  
  }
  

}
