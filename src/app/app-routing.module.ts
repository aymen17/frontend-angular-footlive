import { NgModule } from '@angular/core';
import { NotfoundComponent } from './notfound/notfound.component';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { ListTeamsComponent } from './listteam/listteam.component';
import { SigninComponent } from './signin/signin.component';
import { SignupComponent } from './signup/signup.component';
import { TeamstatComponent } from './teamstat/teamstat.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { WidgetSearchBarButtonComponent } from './search/widget-search-bar-button/widget-search-bar-button.component';
import { SearchResultListComponent } from './search/search-result-list/search-result-list.component';
import { SearchResultItemComponent } from './search/search-result-item/search-result-item.component';
import { PlayerdetailsComponent } from './playerdetails/playerdetails.component';
import { SimulationComponent } from './simulation/simulation.component';
import { MonespaceComponent } from './monespace/monespace.component';
import { MatchComponent } from './match/match.component';
 
const routes: Routes = [
	{ path: '', redirectTo: '/home', pathMatch: 'full' },
	{ path: 'home', component: HomeComponent },
	{ path: 'signin', component: SigninComponent },
	{ path: 'signup', component: SignupComponent },
	{ path: 'ligue/:id', component: ListTeamsComponent},
	{ path: 'team/:id', component: TeamstatComponent },
	{ path: 'player/:teamid/:id', component: PlayerdetailsComponent },
	{ path: 'simulation', component: SimulationComponent },
	{path: 'dashboard', component: DashboardComponent},
	{path: 'test',component:WidgetSearchBarButtonComponent},
	{path:'search-results-list',component:SearchResultListComponent},
	{path:'search-results-item',component:SearchResultItemComponent},
	{path:'monespace',component:MonespaceComponent},
	{path:'matchs',component:MatchComponent},
	{path: '404', component: NotfoundComponent},
	{path: '**', redirectTo: '/404'},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})

export class AppRoutingModule { }