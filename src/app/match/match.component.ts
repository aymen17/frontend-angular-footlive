import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { fixtureModel } from '../Model/fixtureModel';
import { TeamService } from '../team.service';

@Component({
  selector: 'app-match',
  templateUrl: './match.component.html',
  styleUrls: ['./match.component.scss']
})
export class MatchComponent implements OnInit {
  fixtures: fixtureModel[];



  constructor(private teamService: TeamService,private router: Router) { }

  ngOnInit(): void {
    this.getFixtures();
  }

  getFixtures(): void {
   
    this.teamService.getfixtures().subscribe(data=>{
      this.fixtures = data;
      console.log(this.fixtures)
      
    });
    
  };

}
