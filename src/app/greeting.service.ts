import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { ResponseAdmin } from './Model/ResponseAdmin.model';

@Injectable({
providedIn: 'root'
})
export class GreetingService {

	private baseUrl = 'https://springrestapi1.herokuapp.com//greet';
	private baseUrl2 ='https://springrestapi1.herokuapp.com';

	constructor(private http: HttpClient) { }

	getByUserRole(): Observable<string>{
		//const url = `${this.baseUrl}/user`;
		//return this.http.get<string>(url, {responseType: 'text' as 'json'})
		return this.http.get<string>(this.baseUrl + '/user', {responseType: 'text' as 'json'}).pipe(map((resp) => {
			return resp;
		}));
	}

	getUsers(): Observable<ResponseAdmin[]>{
		//const url = `${this.baseUrl}/user`;
		//return this.http.get<string>(url, {responseType: 'text' as 'json'})
		return this.http.get<ResponseAdmin[]>(this.baseUrl2 + '/users').pipe(map((resp) => {
			return resp;
		}));
	}
	
	banUser(responseAdmin: ResponseAdmin) {
		console.log(responseAdmin);
		return this.http.post<ResponseAdmin>(this.baseUrl2 + '/banuser', responseAdmin,{headers: new HttpHeaders({ 'Content-Type': 'application/json' }), responseType: 'text' as 'json'}).subscribe(
			res => {
			  console.log(res);
			}
		); 
	}



	getByAdminRole(): Observable<any>{
		return this.http.get<any>(this.baseUrl + '/admin', {responseType: 'text' as 'json'}).pipe(map((resp) => {
			return resp;
		}));
	}

	getByUserOrAdminRole(): Observable<any>{
		return this.http.get<any>(this.baseUrl + '/userOrAdmin', {responseType: 'text' as 'json'}).pipe(map((resp) => {
			return resp;
		}));
	}

	getByAnonymousRole(): Observable<any>{
		return this.http.get<any>(this.baseUrl + '/anonymous', {responseType: 'text' as 'json'}).pipe(map((resp) => {
			return resp;
		}));
	}

}