import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { HttpInterceptorService } from './httpInterceptor.service';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SigninComponent } from './signin/signin.component';
import { SignupComponent } from './signup/signup.component';
import { HomeComponent } from './home/home.component';
import { ListTeamsComponent } from './listteam/listteam.component';
import { NotfoundComponent } from './notfound/notfound.component';
import { TeamstatComponent } from './teamstat/teamstat.component';
import { ChartsModule } from 'ng2-charts';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { DashboardComponent } from './dashboard/dashboard.component';
import { SearchModule } from 'src/app/search/search.module';
import { ReactiveFormsModule } from '@angular/forms';
import { PlayerdetailsComponent } from './playerdetails/playerdetails.component';
import { SimulationComponent } from './simulation/simulation.component';
import { MonespaceComponent } from './monespace/monespace.component';
import { MatchComponent } from './match/match.component';

@NgModule({
  declarations: [
    AppComponent,
    SigninComponent,
    SignupComponent,
    HomeComponent,
    ListTeamsComponent,
    NotfoundComponent,
    TeamstatComponent,
    DashboardComponent,
    PlayerdetailsComponent,
    SimulationComponent,
    MonespaceComponent,
    MatchComponent,
  ],
  imports: [
    ChartsModule,
    BrowserModule,
    ReactiveFormsModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    SearchModule,
    BrowserAnimationsModule
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: HttpInterceptorService,
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
