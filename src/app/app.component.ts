import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AuthService } from './auth.service';
import { GreetingService } from './greeting.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Title } from '@angular/platform-browser';
import { TeamService } from './team.service';
import { TeamlistModel } from './Model/TeamlistModel';
import { TeamstatModel } from './Model/teamstatModel';


@Component({
	selector: 'app-root',
	templateUrl: './app.component.html',
	styleUrls: ['./app.component.css']
})
export class AppComponent {
	

	isSignedin = false;

	signedinUser: string = '';
	teamfav: TeamstatModel ;
	greeting: any[] = [];
	teams: TeamlistModel[];
	listt: any[];
	favorite: string = '';

	constructor(private title:Title,private teamService: TeamService, private route: ActivatedRoute, private router: Router, private http: HttpClient, private authService: AuthService, private greetingService: GreetingService) { }

	ngOnInit() {
		this.title.setTitle('FootLive');
		if (this.authService.isUserSignedin()){
			this.isSignedin = this.authService.isUserSignedin();
		
			this.signedinUser = this.authService.getSignedinUser();
			this.authService.getUser(this.signedinUser).subscribe(data => {
				this.favorite = data.favorite;
	
				this.teamService.getTeamById(this.favorite).subscribe(data => {
					this.teamfav = data[0];
					console.log(this.teamfav)
				})
			})
		}

		this.getAllTeams();
	}
	doSignout() {
		this.authService.signout();
		window.location.reload();
	}
	getAllTeams(): void {
		this.teamService.getAllTeams().subscribe(data => {
			this.teams = data;
			this.listt = this.teams.map(t => t.teamname);
			console.log(this.listt);
		});
	};

}
