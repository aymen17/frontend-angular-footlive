import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/internal/Observable';
import { SearchOption } from './searchOption';
import { BehaviorSubject } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class SearchService {
  private SERVER_URL = 'https:///apirestnode1.herokuapp.com/footlive/';

  constructor(private http: HttpClient) {}

  private selectedOption = new BehaviorSubject<SearchOption>({
    _id: null,
    rank: null,
    leagueid:null,
    leaguename:null,
    leaguecountry:null,
    flag:null,
    teamid:null,
    teamname:null,
    logo:null,
    points:null,
    goalsdiff:null,
    form:null,
    played:null,
    win:null,
    draw:null,
    lose:null,
    goalsfor:null,
    goalsag:null,
    playername:null,
    playerphoto:null,
    statistics:null,
  });



  private selectedOptions = new BehaviorSubject<SearchOption[]>([]);

  option$ = this.selectedOption.asObservable();
  options$ = this.selectedOptions.asObservable();

  isOptionEmpty$: Observable<boolean>;

  isOptionsEmpty$: Observable<boolean>;

  search(q: string): Observable<SearchOption[]> {
    console.log(this.SERVER_URL + 'search/' + q);
    return this.http.get<SearchOption[]>(
      this.SERVER_URL + 'search/' + q
    );
  }

  updateSelectedOption(option: SearchOption) {
    this.selectedOption.next(option);
  }

  updateSelectedOptions(options: SearchOption[]) {
    this.selectedOptions.next(options);
  }
}
