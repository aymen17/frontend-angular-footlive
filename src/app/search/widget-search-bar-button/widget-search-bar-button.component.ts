import { Component, OnInit } from '@angular/core';
import { SearchService } from '../search.service';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-widget-search-bar-button',
  templateUrl: './widget-search-bar-button.component.html',
  styleUrls: ['./widget-search-bar-button.component.scss'],
})
export class WidgetSearchBarButtonComponent implements OnInit {
  constructor(private searchService: SearchService, private router: Router) {}

  ngOnInit(): void {}

  onSubmit(f: NgForm) {
    this.searchService.options$ = this.searchService.search(f.value.search);
    console.log(f.value)
    console.log (this.searchService.options$);
    this.searchService.updateSelectedOption({
      _id: null,
      rank: null,
      leagueid:null,
      leaguename:null,
      leaguecountry:null,
      flag:null,
      teamid:null,
      teamname:null,
      logo:null,
      points:null,
      goalsdiff:null,
      form:null,
      played:null,
      win:null,
      draw:null,
      lose:null,
      goalsfor:null,
      goalsag:null,
      playername:null,
      playerphoto:null,
      statistics:null,
    });

    this.searchService.isOptionsEmpty$ = this.searchService.options$.pipe(
      map((options) => (options.length == 0 ? true : false))
    );

    this.router.navigate(['/search-results-list']);
    f.resetForm();
  }
}
