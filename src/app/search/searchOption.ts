export interface SearchOption {
_id: string;
rank?: Number;
leagueid?:Number;
leaguename?:string;
leaguecountry?:string;
flag?:string;
teamid?:Number;
teamname?:string;
logo?:string;
points?:Number;
goalsdiff?:Number;
form?:String;
played?:Number;
win?:Number;
draw?:Number;
lose?:Number;
goalsfor?:Number;
goalsag?:Number;
playername?:string;
playerphoto?:Number;
playerid?:Number;
statistics?:any[];
}
