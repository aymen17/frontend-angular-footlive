import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { TeamlistModel } from '../Model/TeamlistModel';
import { TeamService } from '../team.service';
import { Request } from '../Model/request.model';
import { Router } from '@angular/router';

@Component({
  selector: 'app-monespace',
  templateUrl: './monespace.component.html',
  styleUrls: ['./monespace.component.scss']
})
export class MonespaceComponent implements OnInit {

  constructor(private teamService: TeamService, private authService: AuthService, private router: Router) { }

  username: string = '';
  password: string = '';
  email: string = '';
  favorite: string = '';
  isSignedin = false;
  signedinUser: string = '';
  request: Request=new Request();
  teams: TeamlistModel[];
  teamsligue1: TeamlistModel[];
  teamsseriea: TeamlistModel[];
  teamspremierleague: TeamlistModel[];
  teamsligua: TeamlistModel[];
  teamsbundesligua: TeamlistModel[];
  error: string = '';
  success: string = '';

  ngOnInit(): void {
    this.isSignedin = this.authService.isUserSignedin();
    this.signedinUser = this.authService.getSignedinUser();

    this.username = this.signedinUser;
    this.authService.getUser(this.signedinUser).subscribe(data => {
      
      this.email=data.email;
      this.password=data.userPwd;
      this.favorite=data.favorite;
     
    })
  
    


    if (!this.authService.isUserSignedin()) {
      this.router.navigateByUrl('signin');
    }
    this.router.routeReuseStrategy.shouldReuseRoute = () => false;
    this.teamService.getStanding(61).subscribe(data => {
      this.teamsligue1 = data;
    })
    this.teamService.getStanding(135).subscribe(data => {
      this.teamsseriea = data;
    })
    this.teamService.getStanding(78).subscribe(data => {
      this.teamsbundesligua = data;
    })
    this.teamService.getStanding(140).subscribe(data => {
      this.teamsligua = data;
    })
    this.teamService.getStanding(39).subscribe(data => {
      this.teamspremierleague = data;
    })
  }

  getStanding(id: any): void {

    this.teamService.getStanding(id).subscribe(data => {
      this.teams = data;

    });

  };
  editProfile() {
    if (this.username !== '' && this.username !== null && this.password !== '' && this.password !== null && this.email !== '' && this.email !== null) {
      const request: Request = { userName: this.username, userPwd: this.password, email: this.email, favorite: this.favorite };
      console.log(request);
      this.authService.updateuser(request).subscribe((result) => {
        //console.log(result);
        //this.success = 'Signup successful';
        this.success = result;


        this.router.navigateByUrl('signin');
      }, (err) => {
        //console.log(err);
        this.error = 'Something went wrong during signup';
      });
    } else {
      this.error = 'All fields are mandatory';
    }
  }

}
