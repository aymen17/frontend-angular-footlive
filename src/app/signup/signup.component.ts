import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { Request } from '../Model/request.model';
import { catchError } from 'rxjs/operators';
import { Router } from '@angular/router';
import { TeamService } from '../team.service';
import { TeamlistModel } from '../Model/TeamlistModel';


@Component({
selector: 'app-signup',
templateUrl: './signup.component.html',
styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {
	

	constructor(private authService: AuthService, private router:Router,private teamService:TeamService) { }

	username: string = '';
	password: string = '';
	email: string = '';
	favorite: string = '';
	

	teams: TeamlistModel[];
	teamsligue1: TeamlistModel[];
	teamsseriea: TeamlistModel[];
	teamspremierleague: TeamlistModel[];
	teamsligua: TeamlistModel[];
	teamsbundesligua: TeamlistModel[];
	

	error: string = '';
	success: string = '';

	ngOnInit(): void {
		
		this.teamService.getStanding(61).subscribe(data=>{
			this.teamsligue1 = data;
		})
		this.teamService.getStanding(135).subscribe(data=>{
			this.teamsseriea = data;
		})
		this.teamService.getStanding(78).subscribe(data=>{
			this.teamsbundesligua = data;
		})
		this.teamService.getStanding(140).subscribe(data=>{
			this.teamsligua = data;
		})
		this.teamService.getStanding(39).subscribe(data=>{
			this.teamspremierleague = data;
		})
	}
	
	

	  getStanding(id:any): void {
		
		this.teamService.getStanding(id).subscribe(data=>{
		  this.teams = data;
		
		});
		
	  };
		
	 
	

	doSignup() {
		if(this.username !== '' && this.username !== null && this.password !== '' && this.password !== null && this.email !== '' && this.email !== null ) {
			const request: Request = { userName: this.username, userPwd: this.password,email: this.email, favorite: this.favorite };
console.log(request.favorite);
			this.authService.signup(request).subscribe((result)=> {
				//console.log(result);
				//this.success = 'Signup successful';
				this.success = result;
				
			
				this.router.navigateByUrl('signin');
			}, (err) => {
				//console.log(err);
				this.error = 'Something went wrong during signup';
			});
		} else {
			this.error = 'All fields are mandatory';
		}
	}

}