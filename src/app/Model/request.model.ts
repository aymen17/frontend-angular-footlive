export class Request {
	userName: string;
	userPwd: string;
	email?:string;
	favorite?:string;
	roles?: string[];
}