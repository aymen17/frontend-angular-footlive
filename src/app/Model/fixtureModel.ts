export class fixtureModel {
	_id: string;
	date:Number;
    month: Number;
	year:Number;
	hours:Number;
	minutes:Number;
	leagueid:Number;
	leaguename:string;
	leaguecountry:string;
	round:string;
	teamhomeid:Number;
	teamhomename:string;
	teamhomelogo:string;
	teamawayid:Number;
	teamawayname:string;
	teamawaylogo:string;
}