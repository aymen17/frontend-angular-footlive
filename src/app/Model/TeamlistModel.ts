export class TeamlistModel {
	_id: string;
	leagueid:Number;
    rank: Number;
	teamid:Number;
	teamname:string;
	logo:string;
	points:Number;
	goalsdiff:Number;
	form:String;
	played:Number;
	win:Number;
	draw:Number;
	lose:Number;
	goalsfor:Number;
	goalsag:Number;
}