export class PlayerlistModel {
	_id: string;
    playerid:Number;
    teamid:Number;
    leagueid:Number;
    playername:string;
    playerfirstname:string;
    playerlastname:string;
    playerage:string;
    playerdateofbirth:Date;
    playerplaceofbirth:string;
    playercountryofbirth:string;
    playernationality:string;
    playerheight:string;
    playerweight:string;
    playerstatus:string;
    playerphoto:string;
    statistics?:any[];
}