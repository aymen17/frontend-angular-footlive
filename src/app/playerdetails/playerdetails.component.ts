import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Route } from '@angular/router';
import { ChartDataSets, ChartType } from 'chart.js';
import { Color, Label } from 'ng2-charts';
import { PlayerlistModel } from '../Model/PlayerlisModel';
import { TeamService } from '../team.service';

@Component({
  selector: 'app-playerdetails',
  templateUrl: './playerdetails.component.html',
  styleUrls: ['./playerdetails.component.scss']
})
export class PlayerdetailsComponent implements OnInit {
  chartDatasets: any[];
  chartDatasets1: any[];
  chartDatasetspasses: any[];
  chartDatasetsbuts: any[];
  chartDatasetstackles: any[];
  chartDatasetsduels: any[];
  chartDatasetsgardienbut: any[];
  note:any;

  player: PlayerlistModel = new PlayerlistModel();
  barChartLabels: Label[];
  barChartLabels1: Label[];
  barChartLabelspasses: Label[];
  barChartLabelsbuts: Label[];
  barChartLabelstackles: Label[];
  barChartLabelsduels: Label[];
  barChartLabelsgardienbut: Label[];

  barChartLegend = true;
  barChartPlugins = [];
  barChartType: ChartType = 'bar';
  barChartData: ChartDataSets[] = [

  ];
  barChartData1: ChartDataSets[] = [

  ];
  barChartDatapasses: ChartDataSets[] = [

  ];
  barChartDatabuts: ChartDataSets[] = [

  ];

  barChartDatatackles: ChartDataSets[] = [

  ];
  barChartDataduels: ChartDataSets[] = [

  ];
  barChartDatagardienbut: ChartDataSets[] = [

  ];

  public barChartColors: Color[] = [
    { backgroundColor: ['#0079B4','#06A51F','#A51B06'] },
  ]
  public barChartOptions: any = {
    responsive: true,
    title: {
      text: 'Tirs',
      display: true,
       fontColor: 'white',
    },
    scales: {
      yAxes: [{
        ticks: {
          fontColor: "white",
          reverse: false,
          //fontSize: 18,
          //stepSize: 1,
          beginAtZero: true
        }
      }],
      xAxes: [{
        ticks: {
          fontColor: "white",
          // fontSize: 14,
          // stepSize: 1,
          beginAtZero: true
        }
      }],
      

    },

    legend: { position: 'left', labels: { fontColor: 'white' } },
    barChartLabels: { backgroundColor: 'white' }

  }

  public barChartOptions1: any = {
    responsive: true,
    title: {
      text: 'Dribbles',
      display: true,
       fontColor: 'white',
    },
    scales: {
      yAxes: [{
        ticks: {
          fontColor: "white",
          reverse: false,
          //fontSize: 18,
          //stepSize: 1,
          beginAtZero: true
        }
      }],
      xAxes: [{
        ticks: {
          fontColor: "white",
          // fontSize: 14,
          // stepSize: 1,
          beginAtZero: true
        }
      }],
      

    },
    

    legend: { position: 'left', labels: { fontColor: 'white' } },
    barChartLabels: { backgroundColor: 'white' }

  }

  public barChartOptionspasses: any = {
    responsive: true,
    title: {
      text: 'Passes',
      display: true,
       fontColor: 'white',
    },
    scales: {
      yAxes: [{
        id: 'y-axis-0',
        position: 'left',
        ticks: {
          fontColor: "white",
          reverse: false,
          max: 100,
          //fontSize: 18,
          //stepSize: 1,
          beginAtZero: true
        },
      },{
          id: 'y-axis-1',
          position: 'right',
          ticks: {
            max: 80,
            fontColor: "white",
            reverse: false,
            //fontSize: 18,
            //stepSize: 1,
            beginAtZero: true,
            
          },
        }

    ],
      xAxes: [{
        ticks: {
          fontColor: "white",
          // fontSize: 14,
          // stepSize: 1,
          beginAtZero: true
        }
      }],
      

    },
    

    legend: { position: 'left', labels: { fontColor: 'white' } },
    barChartLabels: { backgroundColor: 'white' }

  }

  public barChartOptionsbuts: any = {
    responsive: true,
    title: {
      text: 'Buts / Passes décisives',
      display: true,
       fontColor: 'white',
    },
    scales: {
      yAxes: [{
        id: 'y-axis-0',
        position: 'left',
        ticks: {
          fontColor: "white",
          reverse: false,
          
          //fontSize: 18,
          //stepSize: 1,
          beginAtZero: true
        },
      }

    ],
      xAxes: [{
        ticks: {
          fontColor: "white",
          // fontSize: 14,
          // stepSize: 1,
          beginAtZero: true
        }
      }],
      

    },
    
    legend: { position: 'left', labels: { fontColor: 'white' } },
    barChartLabels: { backgroundColor: 'white' }

  }

  public barChartOptionstackles: any = {
    responsive: true,
    title: {
      text: 'Tacles',
      display: true,
       fontColor: 'white',
    },
    scales: {
      yAxes: [{
        id: 'y-axis-0',
        position: 'left',
        ticks: {
          fontColor: "white",
          reverse: false,
          
          //fontSize: 18,
          //stepSize: 1,
          beginAtZero: true
        },
      }

    ],
      xAxes: [{
        ticks: {
          fontColor: "white",
          // fontSize: 14,
          // stepSize: 1,
          beginAtZero: true
        }
      }],
      

    },
    
    legend: { position: 'left', labels: { fontColor: 'white' } },
    barChartLabels: { backgroundColor: 'white' }

  }

  public barChartOptionsduels: any = {
    responsive: true,
    title: {
      text: 'Duels',
      display: true,
       fontColor: 'white',
    },
    scales: {
      yAxes: [{
        id: 'y-axis-0',
        position: 'left',
        ticks: {
          fontColor: "white",
          reverse: false,
          
          //fontSize: 18,
          //stepSize: 1,
          beginAtZero: true
        },
      }

    ],
      xAxes: [{
        ticks: {
          fontColor: "white",
          // fontSize: 14,
          // stepSize: 1,
          beginAtZero: true
        }
      }],
      

    },
    
    legend: { position: 'left', labels: { fontColor: 'white' } },
    barChartLabels: { backgroundColor: 'white' }

  }
  public barChartOptionsgardienbut: any = {
    responsive: true,
    title: {
      text: 'Buts',
      display: true,
       fontColor: 'white',
    },
    scales: {
      yAxes: [{
        id: 'y-axis-0',
        position: 'left',
        ticks: {
          fontColor: "white",
          reverse: false,
          
          //fontSize: 18,
          //stepSize: 1,
          beginAtZero: true
        },
      }

    ],
      xAxes: [{
        ticks: {
          fontColor: "white",
          // fontSize: 14,
          // stepSize: 1,
          beginAtZero: true
        }
      }],
      

    },
    
    legend: { position: 'left', labels: { fontColor: 'white' } },
    barChartLabels: { backgroundColor: 'white' }

  }
  
  

  constructor(private route: ActivatedRoute, private http: HttpClient,private teamService:TeamService) { }

  ngOnInit(): void {
    this.getplayerById();
  }


  getplayerById(): void {
    const id = this.route.snapshot.paramMap.get('id');
    const teamid = this.route.snapshot.paramMap.get('teamid');
    console.log(id);
    this.teamService.getPlayerById(teamid,id).subscribe(p => {
      this.player=p[0];
      this.note= parseFloat(p[0].statistics[0].games.rating).toFixed(2);
      this.chartDatasets=[ p[0].statistics[0].shots.total , p[0].statistics[0].shots.on ]
      this.barChartData = [
        { data: this.chartDatasets }
      ];
      this.barChartLabels = ["Total","Cadrés"];

      this.chartDatasets1=[ p[0].statistics[0].dribbles.attempts , p[0].statistics[0].dribbles.success ]
      this.barChartData1 = [
        { data: this.chartDatasets1 }
      ];
      this.barChartLabels1 = ["Total","Réussis"];

      this.chartDatasetspasses=[ p[0].statistics[0].passes.accuracy , p[0].statistics[0].passes.key]
      
      this.barChartDatapasses = [
        { data: this.chartDatasetspasses}
      ];
      this.barChartLabelspasses = ["Précision en %","Passes clés"];

      this.chartDatasetsbuts=[ p[0].statistics[0].goals.total , p[0].statistics[0].goals.assists ]
      this.barChartDatabuts = [
        { data: this.chartDatasetsbuts }
      ];
      this.barChartLabelsbuts = ["Buts","Passes décisives"];

      this.chartDatasetstackles=[ p[0].statistics[0].tackles.total , p[0].statistics[0].tackles.interceptions ]
      this.barChartDatatackles = [
        { data: this.chartDatasetstackles }
      ];
      this.barChartLabelstackles = ["Total","Interceptions"];

      this.chartDatasetsduels=[ p[0].statistics[0].duels.total , p[0].statistics[0].duels.won ]
      this.barChartDataduels = [
        { data: this.chartDatasetsduels }
      ];
      this.barChartLabelsduels = ["Total","Gagnés"];

      this.chartDatasetsgardienbut=[ p[0].statistics[0].goals.conceded , p[0].statistics[0].goals.saves ]
      this.barChartDatagardienbut = [
        { data: this.chartDatasetsgardienbut }
      ];
      this.barChartLabelsgardienbut = ["Concédés","Sauvés"];
      
      
      

    }
    );
  };

}
