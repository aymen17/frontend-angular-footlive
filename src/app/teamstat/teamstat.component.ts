import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthService } from '../auth.service';
import { TeamService } from '../team.service';
import { TeamstatModel } from '../Model/teamstatModel';
import { GraphiquesService } from '../graphiques.service';
import { Observable } from 'rxjs';
import { MultiDataSet, Label, Color } from 'ng2-charts';
import { ChartDataSets, ChartOptions, ChartType } from 'chart.js';
import { PieModel } from '../Model/PieModel';
import { PlayerlistModel } from '../Model/PlayerlisModel';

@Component({
  selector: 'app-teamstat',
  templateUrl: './teamstat.component.html',
  styleUrls: ['./teamstat.component.css']
})
export class TeamstatComponent implements OnInit {

  chartType = 'pie';
  isSignedin = false;
  signedinUser: string = '';
  team: TeamstatModel = new TeamstatModel();
  chartDatasets: any[];
  usingSpread: string [];
  players: PlayerlistModel[];

  barChartLabels: Label[];
  barChartType: ChartType = 'horizontalBar';
  barChartLegend = true;
  barChartPlugins = [];
  barChartData: ChartDataSets[] = [

  ];

  doughnutChartLabels: Label[];
  doughnutChartData: MultiDataSet = [];
  doughnutChartType: ChartType = 'doughnut';
  public barChartColors: Color[] = [
    { backgroundColor: '#0079B4' },
  ]

  public doughnutChartColors: any[] = [{
    backgroundColor: ["#ADC9E0",  "#4480B1", "#274460", "#101E29"],
    borderColor: [
      "#0D0D0D",
      "#0D0D0D",
      "#0D0D0D",
      "#0D0D0D",
      "#0D0D0D",
      "#0D0D0D",
      "#0D0D0D",
      "#0D0D0D",
      "#0D0D0D",
      "#0D0D0D",
      "#0D0D0D",
      "#0D0D0D",
      "#0D0D0D"
    ]
  }];

  public doughnutChartOptions: any = {
    responsive: true,
    title: {
      text: 'Age des joueurs',
      display: true,
       fontColor: 'white',
    },
    scales: {

      x: {
        stacked: true,
        fontColor: "green"
      },
      y: {
        stacked: true
      }

    },

    legend: { position: 'left', labels: { fontColor: 'white' } },
    barChartLabels: { backgroundColor: 'white' }

  }

  public barChartOptions: any = {
    responsive: true,
    title: {
      text: 'Nationalités',
      display: true,
       fontColor: 'white',
    },
    scales: {
      yAxes: [{
        ticks: {
          fontColor: "white",
          reverse: true,
          //fontSize: 18,
          //stepSize: 1,
          beginAtZero: true
        }
      }],
      xAxes: [{
        ticks: {
          fontColor: "white",
          // fontSize: 14,
          // stepSize: 1,
          beginAtZero: true
        }
      }],
      x: {
        stacked: true,
        fontColor: "green"
      },
      y: {
        stacked: true
      }

    },

    legend: { position: 'left', labels: { fontColor: 'white' } },
    barChartLabels: { backgroundColor: 'white' }

  }
  constructor(private route: ActivatedRoute, private http: HttpClient, private authService: AuthService, private teamService: TeamService, private router: Router, private graphiquesService: GraphiquesService) { }

  ngOnInit(): void {

    this.getStatTeamByIdNat();
    this.getStatTeamByIdAge();
    this.isSignedin = this.authService.isUserSignedin();
    this.signedinUser = this.authService.getSignedinUser();

    if (!this.authService.isUserSignedin()) {
      this.router.navigateByUrl('signin');
    }
    this.getteamById();
    this.getAllPlayers();
  }

  getteamById(): void {
    const id = this.route.snapshot.paramMap.get('id');
    this.teamService.getTeamById(id).subscribe(data => {
      this.team = data[0];
      console.log(id);
      this.usingSpread= [...this.team.form];
      this.usingSpread=this.usingSpread.slice(-8)
      

    }
    );
  };
  getAllPlayers(): void {
    const id = this.route.snapshot.paramMap.get('id');
    this.teamService.getAllPlayers(id).subscribe(data=>{
      this.players = data;
      console.log(this.players);
      
      
      
      
    });
  };

  getStatTeamByIdAge(): void {
    const id = this.route.snapshot.paramMap.get('id');
    console.log("verif"+id)
    this.graphiquesService.getStatTeamByIdAge(id).subscribe(o => {
      console.log("hey"+o);
      this.doughnutChartData = Object.values(o);
      this.doughnutChartLabels = Object.keys(o);

    }
    );
  };

  getStatTeamByIdNat(): void {
    const id = this.route.snapshot.paramMap.get('id');
    this.graphiquesService.getStatTeamByIdNat(id).subscribe(o => {
      
      this.chartDatasets = Object.values(o);
      this.barChartData = [
        { data: this.chartDatasets, label: "Pays" }
      ];
      this.barChartLabels = Object.keys(o);
    }
    );
  };

}


