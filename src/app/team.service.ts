import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';

import { TeamlistModel } from './Model/TeamlistModel';
import { TeamstatModel } from './Model/teamstatModel';
import { PlayerlistModel } from './Model/PlayerlisModel';
import { ScoreModel } from './Model/score.Model';
import { fixtureModel } from './Model/fixtureModel';


@Injectable({
  providedIn: 'root'
})
export class TeamService {
  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  };
  constructor(private http: HttpClient) { }

  baseurl: string = "https:///apirestnode1.herokuapp.com/";
  baseUrl2 = ' https://springrestapi1.herokuapp.com/';

  getAllTeams(){
    return this.http.get<TeamlistModel[]>(this.baseurl + 'footlive/leagues');
  }

  getfixtures(){
    return this.http.get<fixtureModel[]>(this.baseurl + 'footlive/fixtures');
  }

  getStanding(id:any){
    return this.http.get<TeamlistModel[]>(this.baseurl + 'footlive/leaguestanding/'+`${id}`).pipe(
      
      catchError(this.handleError<TeamlistModel[]>(`getteam id=${id}`))
    );;
  }

  getAllPlayers(id:any){
    return this.http.get<PlayerlistModel[]>(this.baseurl + 'footlive/player/'+`${id}`).pipe(
      
      catchError(this.handleError<PlayerlistModel[]>(`getteam id=${id}`))
    );;
  }

  getTeamById(id:any): Observable<any>{
   
    return this.http.get<any>(this.baseurl + 'footlive/teams/'+`${id}`).pipe(
      
      catchError(this.handleError<TeamstatModel>(`getteam id=${id}`))
    );;

  }

  getScore(id1:any,id2:any): any{
   
    return this.http.get<ScoreModel>(this.baseUrl2 + 'footlive/simulation/'+`${id1}`+'/'+`${id2}`).pipe(
      
      catchError(this.handleError<ScoreModel>(`getid id1=${id1} id2=${id2}`))
    );;

  }



  getPlayerById(teamid:any,id:any): Observable<any>{
    console.log(teamid);
   
    return this.http.get<any>(this.baseurl + 'footlive/'+`${teamid}`+'/playerdetails/'+`${id}`).pipe(
      
      catchError(this.handleError<TeamstatModel>(`getteam id=${id}`))
    );;

  }


  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      this.log(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

  /** Log a HeroService message with the MessageService */
  private log(message: string) {
    console.log(message);
  }
}