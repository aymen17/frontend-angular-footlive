import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { ScoreModel } from '../Model/score.Model';
import { TeamlistModel } from '../Model/TeamlistModel';
import { TeamstatModel } from '../Model/teamstatModel';
import { TeamService } from '../team.service';

@Component({
  selector: 'app-simulation',
  templateUrl: './simulation.component.html',
  styleUrls: ['./simulation.component.scss']
})
export class SimulationComponent implements OnInit {
  teams: TeamlistModel[];
  domicile:TeamstatModel;
  scorerhome:string[];
  scoreraway:string[];
  script:string[];
  exterieur:TeamstatModel;
  score:ScoreModel;
  teamsligue1: TeamlistModel[];
  teamsseriea: TeamlistModel[];
  teamspremierleague: TeamlistModel[];
  teamsligua: TeamlistModel[];
  teamsbundesligua: TeamlistModel[];


  constructor(public fb: FormBuilder,private teamService: TeamService, private router: Router) { }

  oppoSuitsForm = this.fb.group({
    t1: [''],
    t2:['']
  })
  


  ngOnInit(): void {
    this.getAllTeams();

    this.teamService.getStanding(61).subscribe(data => {
      this.teamsligue1 = data;
    })
    this.teamService.getStanding(135).subscribe(data => {
      this.teamsseriea = data;
    })
    this.teamService.getStanding(78).subscribe(data => {
      this.teamsbundesligua = data;
    })
    this.teamService.getStanding(140).subscribe(data => {
      this.teamsligua = data;
    })
    this.teamService.getStanding(39).subscribe(data => {
      this.teamspremierleague = data;
    })
    
  }

  onSubmit() {
    let a=JSON.stringify(this.oppoSuitsForm.value);
    console.log(this.oppoSuitsForm.value.t1);
      console.log(this.oppoSuitsForm.value.t2);
    this.teamService.getTeamById(this.oppoSuitsForm.value.t1).subscribe(data => {
      this.domicile = data[0];
      console.log(data[0])
    }
    )  
    this.teamService.getTeamById(this.oppoSuitsForm.value.t2).subscribe(data => {
      this.exterieur = data[0];
    }
    )
    this.score=this.teamService.getScore(this.oppoSuitsForm.value.t1,this.oppoSuitsForm.value.t2).subscribe(data =>{
this.score=data;
this.scorerhome= this.score.ScorerHome.split(";");
this.scoreraway= this.score.ScorerAway.split(";");
this.script=this.score.script.split("&&")
console.log(this.script)
this.scorerhome.pop();
this.scoreraway.pop();
console.log(this.scorerhome)
    });
    console.log(this.score);
    this.displayStyle = "block"; 
    
  }

  getAllTeams(): void {
    this.teamService.getAllTeams().subscribe(data=>{
      this.teams = data;
      
    });
  };

  
  displayStyle = "none";
  displayStyle2 = "none";
  
  openPopup() {
    
    this.displayStyle = "block";
  }
  closePopup() {
    this.displayStyle = "none";
  }

  openPopup2() {
    
    this.displayStyle2 = "block";
  }
  closePopup2() {
    this.displayStyle2 = "none";
  }

}
