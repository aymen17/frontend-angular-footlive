import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AuthService } from '../auth.service';
import { TeamlistModel } from '../Model/TeamlistModel';
import { TeamService } from '../team.service';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder } from '@angular/forms';


@Component({
  selector: 'app-listteam',
  templateUrl: './listteam.component.html',
  styleUrls: ['./listteam.component.css']
})
export class ListTeamsComponent implements OnInit {
  isSignedin = false;

  signedinUser: string = '';
  id: any;
  leaguename: any;

  greeting: any[] = [];

  teams: TeamlistModel[];
  teams$;

  constructor(public fb: FormBuilder, private route: ActivatedRoute, private http: HttpClient, private authService: AuthService, private teamService: TeamService, private router: Router) { }

  ngOnInit() {
    this.isSignedin = this.authService.isUserSignedin();
    this.signedinUser = this.authService.getSignedinUser();

    if (!this.authService.isUserSignedin()) {
      this.router.navigateByUrl('signin');
    }
    this.router.routeReuseStrategy.shouldReuseRoute = () => false;

    this.getStanding();
  }

  getStanding(): void {
    this.id = this.route.snapshot.paramMap.get('id');
    this.leaguenameget();
    this.teamService.getStanding(this.id).subscribe(data => {
      this.teams = data;
      this.teams$ = data;
    });

  };

  leaguenameget() {
    if (this.id == 61) {
      this.leaguename = "Ligue 1 Uber Eats - Ligue de Football Professionnel"
    } else if (this.id == 39) {
      this.leaguename = "Premier League"
    } else if (this.id == 135) {
      this.leaguename = "Serie A"
    } else if (this.id == 140) {
      this.leaguename = "La Liga Santander"
    } else if (this.id == 78) {
      this.leaguename = "Bundesliga";

    }
  }
}
